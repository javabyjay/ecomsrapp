package in.javabyraghu.jay.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class CategoryNotFoundException extends RuntimeException {
  public CategoryNotFoundException() {
  }

  public CategoryNotFoundException(String message) {
    super(message);
  }
}
