package in.javabyraghu.jay.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.javabyraghu.jay.entity.Category;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
