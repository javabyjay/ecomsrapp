package in.javabyraghu.jay.service;

import in.javabyraghu.jay.entity.Category;
import java.lang.Long;
import java.util.List;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
public interface ICategoryService {
	Long saveCategory(Category category);

	void updateCategory(Category category);

	void deleteCategory(Long id);

	Category getOneCategory(Long id);

	List<Category> getAllCategorys();
}
